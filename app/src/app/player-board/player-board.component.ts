import { Randomizer } from './../models/randomizer.model';
import { Board } from './../models/board.model';
import { PlayerType } from './../enums/player-type.enum';
import { GameService } from './../services/game-service';
import { Guid } from './../models/guid.model';
import { Component, Input, ViewChildren, AfterViewInit, ChangeDetectorRef, QueryList, ElementRef } from '@angular/core';

@Component({
  selector: 'app-player-board',
  templateUrl: './player-board.component.html',
  styleUrls: ['./player-board.component.css']
})
export class PlayerBoardComponent implements AfterViewInit {


  @ViewChildren('rect') rects: QueryList<ElementRef>;
  @ViewChildren('fire') fires: QueryList<ElementRef>;
  @ViewChildren('dot') dotes: QueryList<ElementRef>;

  @Input() public playerType: PlayerType;

  playerName = 'Player';
  playerIdent;
  board: Board;
  movesCounter: number;
  gameService: GameService;
  detector: ChangeDetectorRef;
  fireArray: Array<ElementRef>;
  dotesArray: Array<ElementRef>;

  constructor(gameService: GameService, detector: ChangeDetectorRef) {
    this.detector = detector;
    this.gameService = gameService;
    this.playerIdent = Guid.MakeNew();
  }


  takingFire(cell: string): boolean {

    this.movesCounter++;
    const gotcha = this.board.hasShip(cell, this.fireArray, this.dotesArray);
    if (gotcha) {
      if (!this.board.isNotGameOver()) {
        this.gameService.isNotGameOver = false;
      }
    }
    return gotcha;
  }

  ngAfterViewInit() {
    this.gameService.addPlayer(this);
    this.fireArray = this.fires.toArray();
    this.dotesArray = this.dotes.toArray();
  }

  setPlayerName(playerName: string) {
    this.playerName = playerName;
    this.detector.detectChanges();
  }

  initBoard() {
    this.board = new Board();
    if (this.playerType === PlayerType.Human) {
      this.board.show(this.rects.toArray());
    }
  }

  move() {
    this.gameService.helpMessage = 'Очередь игрока ' + this.playerName;
    switch (this.playerType) {
      case PlayerType.Human:
        break; // ничего не нужно делать, пользователь сам кликает по клеткам
      case PlayerType.Computer:
        setTimeout(() => {
          this.randomMove();
        }, 1000);
        break;

      default:
        break;
    }
  }

  private randomMove() {
    const index = Randomizer.getRandomInt(0, this.board.availibleMoves.length - 1);
    const cellName = this.board.availibleMoves[index];
    this.fire(cellName);
  }

  private fire(cell: string, clicker?: PlayerBoardComponent) {
    const index = clicker ? clicker.board.availibleMoves.indexOf(cell) : this.board.availibleMoves.indexOf(cell);
    if (index === -1) {
      alert('Такой ход уже был!');
      return;
    }

    if (clicker) {
      if (clicker.playerIdent === this.playerIdent) {
        alert('Бей своих, чтобы чужие боялись?');
        return;
      }
      const ident = this.gameService.getPlayerIdentWhichMoves();
      if (ident !== clicker.playerIdent) {
        alert('вне очереди??');
        return;
      }
    }

    this.board.availibleMoves.splice(index, 1);

    this.gameService.fire(cell, clicker ? clicker.playerIdent : this.playerIdent);
  }

  mouseClick(event: Event) {
    const element = event.target as Element;
    const clicker = this.gameService.getMouseClicker();
    this.fire(element.attributes['inkscape:label'].value, clicker);
  }
}
