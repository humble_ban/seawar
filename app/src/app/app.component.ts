import { PlayerType } from './enums/player-type.enum';
import { GameService } from './services/game-service';
import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  playerTypes = PlayerType;
  gameService: GameService;
  detector: ChangeDetectorRef;
  constructor(gameService: GameService, detector: ChangeDetectorRef) {
    this.detector = detector;
    this.gameService = gameService;
  }
  ngAfterViewInit() {
    this.detector.detectChanges();
  }

}

