import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayerBoardComponent } from './player-board/player-board.component';
import { NameDialogComponent } from './name-dialog/name-dialog.component';
import { MatDialogModule, MatRippleModule, MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { GameService } from './services/game-service';

const materialModules = [
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDialogModule,
];

@NgModule({
  declarations: [
    AppComponent,
    PlayerBoardComponent,
    NameDialogComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    materialModules
  ],
  entryComponents: [
    NameDialogComponent
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
