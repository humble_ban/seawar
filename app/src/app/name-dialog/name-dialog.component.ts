import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-name-dialog',
  templateUrl: './name-dialog.component.html',
  styleUrls: ['./name-dialog.component.css']
})
export class NameDialogComponent {
  name: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<NameDialogComponent>) { }

  keyDownFunction(event) {
    if (event.keyCode === 13) {
      this.close();
    }
  }

  close() {
    if (!this.name) {
      return;
    }
    this.dialogRef.close(this.name);
  }


}
