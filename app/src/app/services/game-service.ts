import { PlayerType } from './../enums/player-type.enum';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { NameDialogComponent } from '../name-dialog/name-dialog.component';
import { PlayerBoardComponent } from '../player-board/player-board.component';
import { Guid } from '../models/guid.model';

@Injectable()
export class GameService {
    helpMessage = 'Пожалуйста, представьтесь!';
    playerList = Array<PlayerBoardComponent>();
    isNotGameOver: boolean;
    isPlayer0Moves = true;
    constructor(private dialog: MatDialog) {

    }
    get message(): string {
        return this.helpMessage;
    }
    async showDialog(): Promise<string> {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.width = '60%';
        dialogConfig.height = '40%';
        const dialogRef = this.dialog.open(NameDialogComponent, dialogConfig);
        return await dialogRef.afterClosed().toPromise();
    }

    getPlayerIdentWhichMoves(): Guid {
        return this.playerList[this.isPlayer0Moves ? 0 : 1].playerIdent;
    }

    getMouseClicker(): PlayerBoardComponent {
        const humanplayers = this.playerList.filter(item => item.playerType !== PlayerType.Computer);
        if (humanplayers.length > 1) {
            throw new Error('пока не продумал логику на двух игроков-людей');
        }
        return humanplayers[0];
    }

    private getPlayersNames() {
        this.playerList.forEach(element => {
            this.getPlayerName(element);
        });
    }

    private initPlayersBoards() {
        this.playerList.forEach(element => {
            element.initBoard();
        });
    }

    private getPlayerName(player: PlayerBoardComponent) {
        switch (player.playerType) {
            case PlayerType.Computer:
                player.setPlayerName('Computer');
                break;
            case PlayerType.Human:
                this.showDialog().then(val => {
                    player.setPlayerName(val);
                    this.helpMessage = 'Очередь игрока ' + val;
                });
                break;
            default:
                throw new Error('неизвестный тип игрока');
        }
    }

    fire(cellName: string, playerGuid: Guid) {
        const attacker = this.playerList.filter(item => item.playerIdent === playerGuid);
        const defender = this.playerList.filter(item => item.playerIdent !== playerGuid);

        if (this.isNotGameOver) {
            if (defender[0].takingFire(cellName)) {
                if (!this.isNotGameOver) {
                    this.weHaveAWinner();
                }
                attacker[0].move();
            } else {
                this.isPlayer0Moves = !this.isPlayer0Moves;
                defender[0].move();
            }
        } else {
            this.weHaveAWinner();
        }
    }

    weHaveAWinner() {
        alert('У нас есть победитель!');
        location.reload();
    }

    addPlayer(playerBoard: PlayerBoardComponent) {
        this.playerList.push(playerBoard);
        if (this.playerList.length === 2) {
            // все игроки добавились, можно начинать
            this.gameInit();
        }
    }

    gameInit(): void {
        this.isNotGameOver = true;
        this.getPlayersNames(); // достаем имена игроков
        this.initPlayersBoards();
        this.playerList[this.isPlayer0Moves ? 0 : 1].move();
    }



}
