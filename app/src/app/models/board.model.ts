import { Randomizer } from './randomizer.model';
import { Ship } from './ship.model';
import { ElementRef } from '@angular/core';

export class Board {
    fieldDim = 10;
    waterSymb = 0;
    shipSymb = 1;
    firedSymd = 2;
    cells: number[][];
    ships: Array<Ship>;
    allLetters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
    allDigits = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    availibleMoves = [];
    missed = 0;
    gotcha = 0;
    constructor() {
        this.cells = [];
        for (let index = 0; index < this.fieldDim; index++) {
            this.cells.push(
                [this.waterSymb,
                this.waterSymb,
                this.waterSymb,
                this.waterSymb,
                this.waterSymb,
                this.waterSymb,
                this.waterSymb,
                this.waterSymb,
                this.waterSymb,
                this.waterSymb]);
        }

        this.ships = [];

        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 4));

        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 3));
        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 3));

        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 2));
        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 2));
        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 2));

        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 1));
        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 1));
        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 1));
        this.ships.push(new Ship(0, 0, (Randomizer.getRandomInt(0, 1) === 1), 1));

        for (let shipIndex = 0; shipIndex < this.ships.length; shipIndex++) {
            this.tryEveryCell(shipIndex);
        }

        this.initMoves();

    }

    private initMoves() {
        this.allLetters.forEach(letter => {
            this.allDigits.forEach(digit => {
                this.availibleMoves.push(letter + digit);
            });
        });
    }

    private tryEveryCell(shipIndex: number) {
        if (Randomizer.getRandomInt(0, 1) === 1) {
            this.tryForwards(shipIndex);
        } else {
            this.tryFromCenter(shipIndex);
        }

    }

    private tryForwards(shipIndex: number) {
        for (let i = 0; i < this.fieldDim; i++) {
            for (let j = 0; j < this.fieldDim; j++) {
                if (Randomizer.getRandomInt(0, 1) === 1) {
                    this.ships[shipIndex].positionX = i;
                    this.ships[shipIndex].positionY = j;
                } else {
                    this.ships[shipIndex].positionX = j;
                    this.ships[shipIndex].positionY = i;
                }

                if (this.canSetShip(shipIndex)) {
                    this.setShip(shipIndex);
                    return;
                }
            }
        }
    }

    private tryFromCenter(shipIndex: number) {
        for (let i = 3; i < this.fieldDim; i++) {
            for (let j = 0; j < this.fieldDim; j++) {
                if (Randomizer.getRandomInt(0, 1) === 1) {
                    this.ships[shipIndex].positionX = i;
                    this.ships[shipIndex].positionY = j;
                } else {
                    this.ships[shipIndex].positionX = j;
                    this.ships[shipIndex].positionY = i;
                }

                if (this.canSetShip(shipIndex)) {
                    this.setShip(shipIndex);
                    return;
                }
            }
        }
    }

    private canSetShip(index: number): boolean {
        // проверяем, что наш корабль попадает в поле
        const ship = this.ships[index];
        if (ship.positionX < 0 || ship.positionY < 0 || this.fieldDim <= ship.positionX || this.fieldDim <= ship.positionY) {
            return false;
        }
        if (ship.isVertical && this.fieldDim <= ship.positionY + ship.size) { return false; }
        if (!ship.isVertical && this.fieldDim <= ship.positionX + ship.size) { return false; }

        // проверяем, что в зоне вокруг корабля никого нет
        // обрезаем зону
        const minX = Math.max(0, ship.positionX - 1);
        const minY = Math.max(0, ship.positionY - 1);
        const maxX = Math.min(this.fieldDim - 1, ship.positionX + 1 + (ship.isVertical ? 0 : ship.size));
        const maxY = Math.min(this.fieldDim - 1, ship.positionY + 1 + (ship.isVertical ? ship.size : 0));


        // сама проверка
        for (let x = minX; x <= maxX; x++) {
            for (let y = minY; y <= maxY; y++) {
                if (this.isShipPresent(x, y)) { return false; }
            }
        }
        return true;
    }

    // проверка клетки на наличие корабля
    private isShipPresent(positionX: number, positionY: number): boolean {
        return this.cells[positionX][positionY] === this.shipSymb;
    }

    // проверка на окончание игры
    isNotGameOver(): boolean {
        let isPresent = false;
        for (let i = 0; i < this.fieldDim; i++) {
            for (let j = 0; j < this.fieldDim; j++) {
                if (this.cells[i][j] === this.shipSymb) {
                    isPresent = true;
                }
            }
        }
        return isPresent;
    }

    // отрисовка кораблей, если нужно
    show(rects: Array<ElementRef>): void {
        for (let i = 0; i < this.fieldDim; i++) {
            for (let j = 0; j < this.fieldDim; j++) {
                if (this.cells[i][j] === 1) {
                    rects[i * 10 + j].nativeElement.style.fill = 'rgb(200, 200, 200)';
                }
            }
        }
    }

    // размещение корабля
    private setShip(index: number): void {
        const ship = this.ships[index];
        if (!ship.isVertical) {
            for (let i = 0; i < ship.size; i++) {
                this.cells[ship.positionX + i][ship.positionY] = 1;
            }
        } else {
            for (let i = 0; i < ship.size; i++) {
                this.cells[ship.positionX][ship.positionY + i] = 1;
            }
        }
    }

    hasShip(cell: string, fires: Array<ElementRef>, dotes: Array<ElementRef>) {

        const i = this.letterToNumber(cell[0]);
        const j = parseInt(cell.substr(1, cell.length), 10) - 1;
        if (this.cells[i][j] === 1) {
            this.cells[i][j] = 2;
            fires[this.gotcha].nativeElement.style = 'font-size: 8';
            fires[this.gotcha].nativeElement.firstChild.attributes[2].value = 10 + j * 9.9;
            fires[this.gotcha].nativeElement.firstChild.attributes[3].value = 18 + i * 9.9;
            this.gotcha += 1;
            return true;
        }
        dotes[this.missed].nativeElement.style = 'font-size: 8';
        dotes[this.missed].nativeElement.firstChild.attributes[2].value = 10 + j * 9.9;
        dotes[this.missed].nativeElement.firstChild.attributes[3].value = 17.5 + i * 9.9;
        this.missed += 1;
        return false;
    }

    letterToNumber(letter: string) {
        switch (letter) {
            case 'a': return 0;
            case 'b': return 1;
            case 'c': return 2;
            case 'd': return 3;
            case 'e': return 4;
            case 'f': return 5;
            case 'g': return 6;
            case 'h': return 7;
            case 'i': return 8;
            case 'j': return 9;
            default:
                throw new Error('неизвестная буква');

        }
    }
}
