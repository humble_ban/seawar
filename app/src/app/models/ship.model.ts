export class Ship {
    positionX: number;
    positionY: number;
    isVertical: boolean;
    size: number;
    public constructor(
        positionX: number,
        positionY: number,
        isVertical: boolean,
        size: number) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.isVertical = isVertical;
        this.size = size;
    }


}
